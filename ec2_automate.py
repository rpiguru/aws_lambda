"""
On EC2, admins should be able to tag this instances.
Then this Lambda function will automatically start and stop them on the tagged schedule.

- Sample:
    Tag: RunSchedule
    Value: Start=08:00,Stop=16:00,Start=17:00,Stop=19:00

"""
import boto3
import datetime


def lambda_handler(event, context):
    print "Started lambda function: ", datetime.datetime.now().strftime("%m/%d/%Y %H:%M:%S")
    ec2 = boto3.client("ec2")
    reservations = ec2.describe_instances()["Reservations"]
    for each_item in reservations:
        for instance in each_item["Instances"]:
            print " ---- "
            _id = instance["InstanceId"]
            state = instance["State"]["Name"]
            # Get schedule value
            schedule = None
            for tag in instance['Tags']:
                if tag['Key'] == 'RunSchedule':
                    schedule = tag['Value']
                    break

            if schedule is None:
                break

            sch_list = [sch.split('=') for sch in schedule.split(',')]
            cur_elapsed = datetime.datetime.now().hour * 60 + datetime.datetime.now().minute

            action = None
            if cur_elapsed < str_to_min(sch_list[0][1]):  # Before the 1st schedule
                if sch_list[0][0] == 'Start':
                    if state == 'running':
                        action = 'stop'
                elif sch_list[0][0] == 'Stop':
                    if state == 'stopped':
                        action = 'start'
            elif cur_elapsed > str_to_min(sch_list[-1][1]):     # After the last schedule
                if sch_list[-1][0] == 'Start':
                    if state == 'stopped':
                        action = 'start'
                elif sch_list[-1][0] == 'Stop':
                    if state == 'running':
                        action = 'stop'
            else:
                #  obtain current step in the schedule list
                cur_step = 0
                for i in range(1, len(sch_list)):
                    if cur_elapsed < str_to_min(sch_list[i][1]):
                        cur_step = i - 1
                        break
                if sch_list[cur_step][0] == 'Start':
                    if state == 'stopped':
                        action = 'start'
                elif sch_list[cur_step][0] == 'Stop':
                    if state == 'running':
                        action = 'stop'

            print "ID: ", _id, "  state: ", state, 'action: ', action
            if action == 'start':
                ec2.start_instances(InstanceIds=[_id, ])
            elif action == 'stop':
                ec2.stop_instances(InstanceIds=[_id, ])


def str_to_min(str_val):
    """
    Convert string value to integer in minutes
    04:15  ->  255
    :param str_val:
    :return:
    """
    tmp = str_val.split(':')
    return int(tmp[0]) * 60 + int(tmp[1])

